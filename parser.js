'use strict';

var http            = require('http'),
    fs              = require('fs'),
    path            = require('path'),

    json2csv        = require('json2csv'),
    htmlparser      = require('htmlparser2');

function Parser() {
    var self = this;

    self.lastTag         = null;
    self.isTitleTag      = null;
    self.isDataTag       = null;
    self.isAfterTBODY    = null;
    self.titles          = [];
    self.values          = [];

    self.handleOpeningTag = function(tagName) {
        if (!self.isAfterTBODY) self.isAfterTBODY = tagName === 'tbody';

        self.isDataTag   = self.isAfterTBODY && tagName === 'td';
        self.isTitleTag  = tagName === 'a' && self.lastTag === 'th';
        self.lastTag     = tagName;
    };

    self.handleParsedText = function(text) {
        if (self.isDataTag) self.values.push(text);
        if (self.isTitleTag) self.titles.push(text);
    };

    self.handleClosingTag = function(tagName) {
        if (tagName === 'td' || tagName === 'th') self.isDataTag = self.isTitleTag = false;
    };

    self._parser = new htmlparser.Parser({
        onopentag:  self.handleOpeningTag,
        ontext:     self.handleParsedText,
        onclosetag: self.handleClosingTag
    }, { decodeEntities: true });
}

Parser.prototype.converter = json2csv;

Parser.prototype.createArrOfParsedData = function() {
    var self = this;

    var result      = [],
        titlesCount = self.titles.length,
        tmp         = {},
        j           = 0;

    for (var i = 0, l = self.values.length; i < l; i++) {
        tmp[self.titles[j]] = self.values[i];
        j++;

        if ((i + 1) % titlesCount === 0) {
            result.push(tmp);
            tmp = {};
            j = 0;
        }
    }

    return result;
};

Parser.prototype.run = function(url, filename, cb) {
    var self = this;

    http.get(url, function(res) {
        console.log('GET: %s', url);

        res
            .setEncoding('utf8')
            .on('data', function(chunk) {
                self._parser.write(chunk);
            })
            .on('end', function() {
                self._parser.end();
                self.writeCSV(filename, cb);
            });

    }).on('error', cb || self.handleError);
};

Parser.prototype.writeCSV = function(filename, cb) {
    var self = this;

    self.converter({
        data:   self.createArrOfParsedData(),
        fields: self.titles
    }, function(err, csv) {
        if (err) return cb ? cb(err) : self.handleError(err);

        fs.writeFile(path.join(__dirname, '/' + filename), csv, function(err) {
            if (err) return cb ? cb(err) : self.handleError(err);
            if (cb) cb(null);

            console.log('csv file created: %s', filename);
        });
    });
};

Parser.prototype.handleError = function(err) {
    console.log(err.message);
    process.exit(1);
};

module.exports = Parser;