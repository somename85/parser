var fs      = require('fs'),
    path    = require('path'),
    assert  = require('chai').assert,

    Parser  = require('../parser'),
    parser  = new Parser(),

    html    = fs.readFileSync(path.join(__dirname + '/mock-page.html'));

describe('parser.js', function() {
    it('should parse data from page', function(done) {
        var expectedResult = '"Lorem ipsum.","Expedita, unde.","Doloremque, est.","Nesciunt, nihil."\n'
            + '"Lorem ipsum dolor.","Facilis, iure quos.","Qui, repudiandae, sint!","A, sed, sint."\n'
            + '"Lorem ipsum dolor.","Error, officiis, reiciendis.","Eaque, itaque, repellendus.","Adipisci, earum, repellendus."\n'
            + '"Lorem ipsum dolor.","At, officiis voluptatum!","Dolor, saepe velit.","Dolorem facilis, voluptates."\n'
            + '"Lorem ipsum dolor.","Expedita, ipsam, quidem!","Exercitationem, sapiente similique!","Cupiditate, officia, ullam?"';

        parser._parser.write(html);
        parser.writeCSV('test.csv', function(err) {
            assert.isNull(err);

            var pathToTestFile = path.join(__dirname + '/../test.csv');

            fs.readFile(pathToTestFile, function(err, data) {
                assert.isNull(err);
                assert.equal(data.toString(), expectedResult);

                fs.unlink(pathToTestFile, done);
            });
        })
    });
});
