'use strict';

var Parser      = require('./parser'),
    parser      = new Parser();

parser.run(getURL(), getFileName(), function(err) {
    if (err) throw err;
});

function getURL() {
    var i = process.argv.indexOf('--u');
    return ~i
        ? process.argv[i + 1]
        : process.env.URL
            ? process.env.URL
            : 'http://mindortrans.donland.ru/Default.aspx?pageid=103322';
}
function getFileName() {
    var i = process.argv.indexOf('--f');
    return ~i
        ? process.argv[i + 1]
        : process.env.URL
            ? process.env.URL
            : 'rostov_2015.csv';
}