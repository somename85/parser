# Simple parser
It should parse one page and create csv file.

### Usage
    node parse_rostov.js
    URL=yourPageUrl FILENAME=yourFileName node parse_rostov.js
    node parse_rostov.js --u yourPageUrl --f yourFileName
    
### Defaults
* Url         - http://mindortrans.donland.ru/Default.aspx?pageid=103322
* Filename    - rostov_2015.csv

### Tests
    mocha
